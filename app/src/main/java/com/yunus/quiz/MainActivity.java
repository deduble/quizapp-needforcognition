package com.yunus.quiz;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;
public class MainActivity extends AppCompatActivity {
    public int score = 0;
    final int NUMBER_OF_Q = 10;

    public int Q1Score, Q2Score,
            Q3Score, Q4Score,
            Q5Score, Q6Score,
            Q7Score, Q8Score,
            Q9Score, Q10Score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void Q1(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = false;
        switch(view.getId()) {
            case R.id.Q1value1:
                if (checked)
                    Q1Score = -2;
                    break;
            case R.id.Q1value2:
                if (checked)
                    Q1Score = -1;
                    break;
            case R.id.Q1value3:
                if (checked)
                    Q1Score = 0;
                    break;
            case R.id.Q1value4:
                if (checked)
                    Q1Score = 1;
                    break;
            case R.id.Q1value5:
                if (checked)
                    Q1Score = 2;
                    break;
        }
        if(reversed){Q1Score = Q1Score * -1;}
    }
    public void Q2(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = false;
        switch(view.getId()) {
            case R.id.Q2value1:
                if (checked)
                    Q2Score = -2;
                break;
            case R.id.Q2value2:
                if (checked)
                    Q2Score = -1;
                break;
            case R.id.Q2value3:
                if (checked)
                    Q2Score = 0;
                break;
            case R.id.Q2value4:
                if (checked)
                    Q2Score = 1;
                break;
            case R.id.Q2value5:
                if (checked)
                    Q2Score = 2;
                break;
        }
        if(reversed){Q2Score = Q2Score * -1;}
    }
    public void Q3(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = true;
        switch(view.getId()) {
            case R.id.Q3value1:
                if (checked)
                    Q3Score = -2;
                break;
            case R.id.Q3value2:
                if (checked)
                    Q3Score = -1;
                break;
            case R.id.Q3value3:
                if (checked)
                    Q3Score = 0;
                break;
            case R.id.Q3value4:
                if (checked)
                    Q3Score = 1;
                break;
            case R.id.Q3value5:
                if (checked)
                    Q3Score = 2;
                break;
        }
        if(reversed){Q3Score = Q3Score * -1;}
    }
    public void Q4(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = true;
        switch(view.getId()) {
            case R.id.Q4value1:
                if (checked)
                    Q4Score = -2;
                break;
            case R.id.Q4value2:
                if (checked)
                    Q4Score = -1;
                break;
            case R.id.Q4value3:
                if (checked)
                    Q4Score = 0;
                break;
            case R.id.Q4value4:
                if (checked)
                    Q4Score = 1;
                break;
            case R.id.Q4value5:
                if (checked)
                    Q4Score = 2;
                break;
        }
        if(reversed){Q4Score = Q4Score * -1;}
    }
    public void Q5(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = true;
        switch(view.getId()) {
            case R.id.Q5value1:
                if (checked)
                    Q5Score = -2;
                break;
            case R.id.Q5value2:
                if (checked)
                    Q5Score = -1;
                break;
            case R.id.Q5value3:
                if (checked)
                    Q5Score = 0;
                break;
            case R.id.Q5value4:
                if (checked)
                    Q5Score = 1;
                break;
            case R.id.Q5value5:
                if (checked)
                    Q5Score = 2;
                break;
        }
        if(reversed){Q5Score = Q5Score * -1;}
    }
    public void Q6(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = false;
        switch(view.getId()) {
            case R.id.Q6value1:
                if (checked)
                    Q6Score = -2;
                break;
            case R.id.Q6value2:
                if (checked)
                    Q6Score = -1;
                break;
            case R.id.Q6value3:
                if (checked)
                    Q6Score = 0;
                break;
            case R.id.Q6value4:
                if (checked)
                    Q6Score = 1;
                break;
            case R.id.Q6value5:
                if (checked)
                    Q6Score = 2;
                break;
        }
        if(reversed){Q6Score = Q6Score * -1;}
    }
    public void Q7(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = true;
        switch(view.getId()) {
            case R.id.Q7value1:
                if (checked)
                    Q7Score = -2;
                break;
            case R.id.Q7value2:
                if (checked)
                    Q7Score = -1;
                break;
            case R.id.Q7value3:
                if (checked)
                    Q7Score = 0;
                break;
            case R.id.Q7value4:
                if (checked)
                    Q7Score = 1;
                break;
            case R.id.Q7value5:
                if (checked)
                    Q7Score = 2;
                break;
        }
        if(reversed){Q7Score = Q7Score * -1;}
    }
    public void Q8(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = false;
        switch(view.getId()) {
            case R.id.Q8value1:
                if (checked)
                    Q8Score = -2;
                break;
            case R.id.Q8value2:
                if (checked)
                    Q8Score = -1;
                break;
            case R.id.Q8value3:
                if (checked)
                    Q8Score = 0;
                break;
            case R.id.Q8value4:
                if (checked)
                    Q8Score = 1;
                break;
            case R.id.Q8value5:
                if (checked)
                    Q8Score = 2;
                break;
        }
        if(reversed){Q8Score = Q8Score * -1;}
    }
    public void Q9(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = false;
        switch(view.getId()) {
            case R.id.Q9value1:
                if (checked)
                    Q9Score = -2;
                break;
            case R.id.Q9value2:
                if (checked)
                    Q9Score = -1;
                break;
            case R.id.Q9value3:
                if (checked)
                    Q9Score = 0;
                break;
            case R.id.Q9value4:
                if (checked)
                    Q9Score = 1;
                break;
            case R.id.Q9value5:
                if (checked)
                    Q9Score = 2;
                break;
        }
        if(reversed){Q9Score = Q9Score * -1;}
    }
    public void Q10(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        boolean reversed = true;
        switch(view.getId()) {
            case R.id.Q10value1:
                if (checked)
                    Q10Score = -2;
                break;
            case R.id.Q10value2:
                if (checked)
                    Q10Score = -1;
                break;
            case R.id.Q10value3:
                if (checked)
                    Q10Score = 0;
                break;
            case R.id.Q10value4:
                if (checked)
                    Q10Score = 1;
                break;
            case R.id.Q10value5:
                if (checked)
                    Q10Score = 2;
                break;
        }
        if(reversed){Q10Score = Q10Score * -1;}
    }
    public void submit(View view){
        final Button button = findViewById(R.id.submit);
        try {
            int duration = Toast.LENGTH_LONG;
            Context context = getApplicationContext();
            score = Q1Score + Q2Score + Q3Score + Q4Score + Q5Score + Q6Score + Q7Score + Q8Score + Q9Score + Q10Score;
            String result;
            if(score<= -10){
                result = "You really do not care about steaming your engines. But worry not! It does not mean you are not smart!";
            }
            else if(score > -10 && score < 10)
            {
                result = "You are moderately using your cognitive resources on your daily life. It is okay, you won't break it if you work it a little bit harder. Don't worry!";
            }
            else{result = "Wow, congratulations! you love to elaborate things for fun! Keep the potato running, you ought to be successful person!";}
            Toast toast = Toast.makeText(context, result, duration);
            toast.show();
            button.setText("Reset");
        }
        catch (NullPointerException ex){
        }
    }
}
